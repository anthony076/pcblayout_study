## 基礎
- Schematic-Drawing --> sch 元件 --> pcb 元件 --> 3D-model

  - 在原理圖中只能放置 sch 元件
    
  - `sch 元件`，是原理圖上的`記號`(Symbol)，用來標記元件的接腳功能
    - sch 元件的 property，會記錄連結的 pcb 元件
  
  - `pcb 元件`，用於紀錄元件接腳在PCB上的`封裝方式`(Footprints，元件接腳映射到PCB版的形狀)，
    - 例如，元件接腳的形狀、位置、大小、形狀、元件接腳之間的距離
    - pcb 元件的 property，會記錄連結的 3D-model

  - `3D-model`，用於繪製元件的實際外型

- 檔案說明

  | 副檔名   | 說明         |
  | -------- | ------------ |
  | *.prjPcb | 專案檔案     |
  | *.SchDoc | 原理圖       |
  | *.PcbDoc | PCB 設計圖   |
  | *.PcbLib | PCBLibrary檔 |
  | *.SchLib | SCHLibrary檔 |

## 同步更新 sch-library 和 schematic
- 以更改元件 的 PCB 封裝名為例
- 在 SCH-Library 中修改 PCB 的封裝方式
  
  <img src="common/update-schdoc-1.png" width=500 height=auto>

- 點擊元件，右鍵選擇 Update Schematic Sheets
  
  <img src="common/update-schdoc-2.png" width=400 height=auto>

## 修改第三方的 PcbLib 庫
- 修改第三方的 PcbLib 庫，並將第三方的 PcbLib 中的元件，添加到自己的專案中

- 例如，在 component 的頁面中，有 connector (0395021004)
  
  <img src="common/add-3rd-pcb-component-1.png" width=500 height=auto>

- step1，將元件放在 Schematic 後，透過 property 查看 pcb 封裝的資訊
  
  檔案位於 C:\Program Files\Altium\AD22\Library\altium-library\footprints\Connector - Molex\PCB - CONNECTOR - MOLEX - MOLEX 395021004.PcbLib

  <img src="common/add-3rd-pcb-component-2.png" width=500 height=auto>

- step2，在硬碟中找到該檔案並開啟 3rd-PCBLibrary 的檔案
  
  <img src="common/add-3rd-pcb-component-3.png" width=500 height=auto>

- step3，切換到 PCBLibrary 頁面，點選目標後，複製元件
  
  <img src="common/add-3rd-pcb-component-4.png" width=500 height=auto>

- step4，點選`專案的 PcbLib 檔案`  > PCBLibrary 頁面 > 貼上複製的元件

  <img src="common/add-3rd-pcb-component-5.png" width=500 height=auto>

  複製完成後，就可將第三方庫的檔案關閉

## 修改第三方的 SchLib 庫
- 修改第三方的 SchLib 庫，並將第三方的 SchLib 中的元件，添加到自己的專案中
  
- step1，在 component 的頁面中，在標題欄點擊右鍵 > Select Columns > 選擇 Library Path

  <img src="common/add-3rd-sch-component-1.png" width=500 height=auto>

  或利用 Library Ref 的值，在 altium 的 Library 目錄中進行搜尋
  
  <img src="common/add-3rd-sch-component-5.png" width=500 height=auto>

- step2，在硬碟中找到該檔案並開啟 3rd-SchLibrary 的檔案
  
  <img src="common/add-3rd-sch-component-2.png" width=500 height=auto>

- step3，切換到 SCH Library 頁面，對目標元件進行複製

  <img src="common/add-3rd-sch-component-3.png" width=400 height=auto>

- step4，點選`專案的 SchLib 檔案` > SCHLibrary 頁面 > 貼上複製的元件

  <img src="common/add-3rd-sch-component-4.png" width=400 height=auto>

  注意，手動複製的 sch 元件不會與 pcb 元件進行連結，需要手動連結



## 快速更換原理圖上的 sch 元件
- 以 更換 connector 為例

  <img src="common/fast-replace-component-in-schematic.png" width=1000 height=auto>
