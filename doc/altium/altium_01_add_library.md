
    - 將Schematic Library 和 PCB Library， 保存在 lib 目錄中

## 添加 built-library (已停止更新)
- 註冊 Altium365
  
  <img src="create-pcb-library/register_altium365.png" width=700 height=auto>
  
- 登入帳號並進行[下載](https://www.altium.com/documentation/other_installers)

- 添加到 Altium 中

  <img src="create-pcb-library/install-built-library.png" width=700 height=auto>

## 添加 3rd-library
- 安裝 開源免費的 altium-library
  - [github](https://github.com/issus/altium-library)
  - [官網](https://altiumlibrary.com/GetStarted)

  - step1，`git clone https://github.com/issus/altium-library`

  - step2，註冊帳號，登入後在` Server Logins > Add New` 添加帳號

    <img src="create-pcb-library/add-free-library-1.png" width=500 height=auto>

  - step3，點擊綠色的 Dblib 進行下載，將下載的 `Celestial Altium Library - altium_library.DbLib` 檔案，保存在 step1 的目錄中

    <img src="create-pcb-library/add-free-library-2.png" width=400 height=auto>

  - step4，在 altium 中，選擇
  
    `Component > Operations > File-based Libraries Perferences`

    <img src="create-pcb-library/add-free-library-3.png" width=500 height=auto>

  - step5，選擇  `Celestial Altium Library - altium_library.DbLib` 檔案

    <img src="create-pcb-library/add-free-library-4.png" width=700 height=auto>

  - step6，若正常載入，可在選單中看見新增的元件庫

    <img src="create-pcb-library/add-free-library-5.png" width=500 height=auto>

  - Troubleshooting，無法顯示 library 內容
    - 安裝 [Microsoft Access Database Engine](https://www.altium.com/documentation/altium-designer/using-database-libraries-with-32-64-bit-altium-design-software-same-computer)

    - 安裝 [MSSQL Native Client](https://altiumlibrary.com/GetStarted/Troubleshooting)

    - 安裝成功後

      <img src="create-pcb-library/add-free-library-6.png" width=300 height=auto>

## Ref
- [舊版官方Library下載](https://techdocs.altium.com/display/ADOH/Download%20Libraries)

- [官方 Library Story](https://designcontent.live.altium.com/)

- [添加庫 @ 台部落](https://www.twblogs.net/a/5b8e40042b7177188343d4b4)