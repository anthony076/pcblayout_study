
## PCB 建庫流程
- 建立元件名
- 放置引腳      (繪製在 頂層， Top-Layer)
- 放置外框      (繪製在 絲印層， Top-Overlay)
- 放置3D外型    (繪製在 機械層， Mechenical-Layer)

## 環境準備
- File > New > Project

- 建立必要檔案
    - Project 右鍵 > Add New to Project > Schematic Library
    - Project 右鍵 > Add New to Project > PCB Library
    - Project 右鍵 > Add New to Project > Schematic
    - Project 右鍵 > Add New to Project > PCB

- 保存檔案
    - 在專案中， 建立 pcb lib sch 三個目錄
    - 將建立的 Schematic 檔案保存在 sch 目錄中
    - 將建立的 PCB 檔案保存在 pcb 目錄中

## PCB 建庫範例 (usb2rs485) - 繪製引腳
- 從 datasheet 檢視元件物理參數
  - 以 [SIT65HVD08](http://www.superesd.com/uploads/20200701/SIT65HVD08-datasheet(EN)_V1.6.pdf) 的 MSOP8 為例

    <img src="create-pcb-library/SIT65HVD08-msop8.png" width=500 height=auto>

  - B1=5.0    引腳最長距離
  - A2=0.65   引腳中心距離
  - B1=5.0    引腳最長距離
  - A=3       芯片長
  - B=3       芯片寬
  - C1=1.1    芯片高
  
- 建立新元件

  <img src="create-pcb-library/create-new-footprint.png" width=750 height=auto>

- 切換到 Top-Layer

- 變更 Grid 樣式

  <img src="create-pcb-library/change_grid_style.png" width=900 height=auto>

- 變更 Grid 的單位
  - 方法1
  
    <img src="create-pcb-library/switch_unit_1.png" width=300 height=auto>

  - 方法2
  
    <img src="create-pcb-library/switch_unit_2.png" width=500 height=auto>

  - 方法3，使用快速鍵 

    `ctrl+G`

- 變更 pad 的預設樣式

  <img src="create-pcb-library/default-pad-style.png" width=700 height=auto>

- 插入 pad

  `Place > Pad`

- 設置吸附格 (snap-grid)，使元件間距等距
  
  - 方法1， `ctrl + shift + G`
  - 方法2，透過 property 修改

    <img src="create-pcb-library/set-snap-grid.png" width=900 height=auto>

- 批量調整間距
  - 選擇類似的 object

    <img src="create-pcb-library/select-same-object-1.png" width=400 height=auto>
    
    調整為 same 並按下 OK

    <img src="create-pcb-library/select-same-object-2.png" width=500 height=auto>

    重新設定容易計算的位置

    <img src="create-pcb-library/select-same-object-3.png" width=300 height=auto>

- 測量位置
  - 按下 `ctrl+m` 進行測量

    <img src="create-pcb-library/measure-position.png" width=200 height=auto>

  - 清除測量

    <img src="create-pcb-library/clear-measure.png" width=200 height=auto>
  
- 將元件中心設置到原點

  `Edit > Set Reference > Center`

## PCB 建庫範例 (usb2rs485) - 繪製絲印外框
- 切換到 Top-Overlay
- 按下 `ctrl+m`，將 snap-grid 設置為 1.5mm (芯片長和寬為3mm)
- place > line 劃出 3*3 的芯片外框
- 按下 `ctrl+m`，將 snap-grid 設置為 0.01mm 
- 將第一腳的位置更改為斜線，或在第一腳的位置添加點

  <img src="create-pcb-library/draw-component-frame.png" width=100 height=auto>

## PCB 建庫範例 (usb2rs485) - 繪製3D外型
- 注意，添加 3d-model 的方法有兩種
  - 方法1，使用 altium 內建的 3d-model，
    - `Tools > Manage 3D Bodies for Library`
    - 簡易的3d方塊，在找不到可用的 3d-model 時可用
  
  - 方法2，使用 第三方的 3d-model
    - [jlcpcb](https://jlcpcb.com/parts)
    - [snapeda](https://www.snapeda.com/home/)
    - [3dcontentcentral](https://www.3dcontentcentral.com/Search.aspx?arg=Electrical%20Components&tagsnavigator=altium&SortBy=match&PageSize=10)
    - [ultralibrarian](https://www.ultralibrarian.com/cad-vendors/altium)
  
- 方法1，以內建的 3d-model 為例
  - [參考](https://www.youtube.com/watch?v=6AU94oTnG68)
    
  - 切換到 Machanicl-Layer

  - 手動添加 3d-model，`Tools > Manage 3D Bodies for Library`

    <img src="create-pcb-library/add-builtin-3d-model-1.png" width=800 height=auto>

  - 配置 3d-model

    <img src="create-pcb-library/add-builtin-3d-model-2.png" width=500 height=auto>

  - 選擇 3d-view

    <img src="create-pcb-library/add-builtin-3d-model-3.png" width=300 height=auto>

  - `shift + 滑鼠左鍵`，檢視結果

    注意，出現圓球後才可以使用滑鼠移動

    <img src="create-pcb-library/add-builtin-3d-model-4.png" width=600 height=auto>

- 方法2，以第三方的 3d-model 為例
  - [參考1](https://www.youtube.com/watch?v=kQRAi17IKX4)
  - [參考2](https://www.youtube.com/watch?v=O7Apm0_DN2E)

## 直接從其他檔案複製 PCB-Library
[參考](https://youtu.be/kQRAi17IKX4?t=173)

## Ref
- [常規pcb建庫方法 @ HD匠](https://www.youtube.com/watch?v=6kOWAgXFrwY)
- [Placing Footprint Pads in Altium Designer | Component Creation](https://www.youtube.com/watch?v=ai-tDuc79Oc)