
## 建立 sch 元件流程
- 建立 sch 元件
- 放置 sch 元件引腳，並建立引腳功能名稱
- 指定封裝，並添加 sch 元件的相關訊息

## 環境準備
- File > New > Project

- 建立必要檔案
    - Project 右鍵 > Add New to Project > Schematic Library
    - Project 右鍵 > Add New to Project > PCB Library
    - Project 右鍵 > Add New to Project > Schematic
    - Project 右鍵 > Add New to Project > PCB

- 保存檔案
    - 在專案中， 建立 pcb lib sch 三個目錄
    - 將建立的 Schematic 檔案保存在 sch 目錄中
    - 將建立的 PCB 檔案保存在 pcb 目錄中

- [建立 pcb 元件庫](altium_02_create_pcb_componet.md)

## 範例 sch 建庫範例 (以 usb2rs485 為例)

- step1，切換到 SCH Library 頁面，並對元件進行更名

  若未出現SCH Library 頁面，可在 `View > Panels > 勾選 SCH Library` 開啟

  <img src="create-sch-library/select-sch-library.png" width=700 height=auto>

- step2，繪製方框和 Pin
  - 繪製外框，`Place > Rectangle`
  - 繪製 Pin，`Place > Pin`
    - 未放置前，按下 `Space 鍵`，使 pin 旋轉，名稱在內，PIN 編號在外
      
    - 未放置前，按下 `Tab 鍵`，修改 pin 的相關屬性
      - 修改 `pin 編號(Designator)`、
      - 修改 `pin 名稱(Name)`，若要顯示上划線，在後方加上\，例如，R\E\ 
      - 修改`電氣類型(Electrical-Type)`
        - Power : 電源、接地 都屬於此類
        - Input
        - Output
  
  - 繪製結果
  
    <img src="create-sch-library/draw-pin.png" width=700 height=auto>

- step3，添加sch元件的訊息

  選擇元件 > Properties > 輸入 Designator、Comment、Descriptor

  <img src="create-sch-library/add-component-info.png" width=900 height=auto>

- step4，添加sch元件對應的pcb元件
  
  在 Properties > Parameters > Add > Footprint > 透過 Browser 選擇對應的 PCB 元件
  <img src="create-sch-library/link-pcb-to-compoent.png" width=900 height=auto>

## Ref 
- [原理圖庫建立方法 @ HD匠](https://www.youtube.com/watch?v=3AWxQvdewuQ)