## 繪製 Schematic 流程
- 添加 sch 元件
- 連接 sch 元件
  - 建立 sch元件之間的連線
  - 通過 net-label 連接相同訊號的元件
  - 添加指令標記

- 修改 sch 元件編號
- 檢查 Schematic

## 環境準備
- File > New > Project

- 建立必要檔案
    - Project 右鍵 > Add New to Project > Schematic Library
    - Project 右鍵 > Add New to Project > PCB Library
    - Project 右鍵 > Add New to Project > Schematic
    - Project 右鍵 > Add New to Project > PCB

- 保存檔案
    - 在專案中， 建立 pcb lib sch 三個目錄
    - 將建立的 Schematic 檔案保存在 sch 目錄中
    - 將建立的 PCB 檔案保存在 pcb 目錄中

- [建立 pcb 元件庫](altium_02_create_pcb_componet.md)

- [建立 sch 元件庫](altium_03_create_sch_componet.md)


## 常用快速鍵
- 旋轉元件，`空白鍵`

- 修改 property，，`TAB鍵`

- 翻轉 
  - 左右鏡像翻轉，`按住滑鼠左鍵 + X鍵`
  - 上下鏡像翻轉，`按住滑鼠左鍵 + Y鍵`

## 常用名詞
- 網路標誌 (Net-Label)，
  
  用於宣告將兩個不封閉線段視為同一個訊號，
  兩個使用相同 Net-Label 的訊號，會被視為是`連接在一起`的

- 指令標記 (Directive)
  - 功能，指令在繪製PCB時，會依照指令的類型自動添加對應的功能

  - 指令，`不檢查指令` (Generic-No-ERC-Directive)

    用於不需要使用的接腳，添加 Generic-No-ERC-Directive 的接腳不會檢查
    是否是懸空腳位

  - 指令，`差分線指令` (Differential-Pair-Directive)

    例如，添加 Differential-Pair-Directive 的差分線指令時，
    會自動將差分線設置為等長，具有相同前綴的 Net-Label，
    會被判定為同一對訊號

## 範例，繪製 Schematic 範例 (以 usb2rs485 為例)
- step1，放置零件

  滑鼠右鍵，有快速選單

- step2，進入畫線模式 (Ctrl + W)

- step3，對相同的訊號放置 Net-Label，表示兩個接腳是連接的
  
  `Place > Net-Label`

- step4，(選用)對需要等長的訊號，放置 Differential-Pair 指令，
  > `Place > Directive > Differential-Pair`

  <img src="draw-schematic/place-differential-pair.png" width=400 height=auto>

- step5，(選用)對不使用的接腳，放置 Generic-No-ERC 指令，
  > `Place > Directive > Generic-No-ERC`

  <img src="draw-schematic/place-noERC.png" width=300 height=auto>

- step6，放置 Power/Gnd 
  - 可使用快速欄中的圖示建立
  
    <img src="draw-schematic/place-power-gnd-1.png" width=400 height=auto>
  
  - 或使用 `Place > Power Port`

    <img src="draw-schematic/place-power-gnd-2.png" width=250 height=auto>

- step7，手動 / 自動 添加元件編號
  - 手動添加元件屬性

    在元件的 Properties 中，Comment 可以自訂義內容，通常用來標記元件規則

    <img src="draw-schematic/add-property-comment.png" width=400 height=auto>

  - 自動添加所有元件編號
    
    執行命令

    <img src="draw-schematic/auto-name-component-1.png" width=400 height=auto>

    執行變更

    <img src="draw-schematic/auto-name-component-2.png" width=500 height=auto>

- step8，驗證 Schematic 是否出現錯誤

  <img src="draw-schematic/validate_schematic.png" width=400 height=auto>

  - 常見錯誤，no driving source，輸入/輸出接腳不匹配

    只有 Output 類型的接腳，才能接到 Input 類型的接腳

  - 常見錯誤，Floating Net-Label

    Net-Label 用於連接訊號，因此 Net-Label 最少需要 2個

  - 常見錯誤，Net XXX contains floating input

    未使用的接腳需要加上 noERC 指令


- step9，最終結果

  <img src="draw-schematic/final-schematic.png" width=500 height=auto>

## Schematic 的產出物，網表 (net-list)
- `元件之間的連接都會形成一個網路`，最小的網路只有兩個端點，
  被連接的接腳，就是該網路上的節點

- 網表有固定格式，只記錄以下資訊
  - 元件訊息，包含元件編號、元件的封裝方式
  - 網路訊息，包含網名、網路引腳，例如，Vcc 網路 + 使用 Vcc 網路有那些引腳
  - 其他屬性，包含元件的模型、網路的屬性、 ... 等

- 透過網表，將 Schematic 的訊息導入到PCB的繪製中

  要繪製 PCB ，只需要網表中的 `元件訊息` 和 `網路訊息`

- 網表是通用的，無論哪一種 PCBLayout 軟體都支援匯入網表
  
- 將 Schematic 匯出網表
  - 開啟 Schematic (*.SchDoc)
  - Design > Netlist for Project > Protel
  - 產生 Generated > Netlist Files > *.NET

- 解讀網表
  - `][ 表示元件`
  ```
  ][        // 每個 ][ 就是一個元件，
  U1        // 元件名
  MSOP10    // 封裝方式
  CH340     // comment
  ```
  - `() 表示網路`
  ```
  ]
  (         // 電源網路
    VCC_5V    // 網路名，代表連接到 VCC_5V 網路的所有節點
    C2-1      // 連接到 C2 元件的第1腳
    C3-1      // 連接到 C3 元件的第1腳
    C4-1
    C5-1
    J_485-1   // 連接到 J_485 元件的第1腳
    J_USB-1
    U1-7      // 連接到 U1 元件的第7腳
    U2-8
  )

  (         // 使用者透過 net-lable 自定義的網路
    USB_P
    J_USB-2
    U1-1
  )

  (         // 使用者未定義的網路，會由系統自動創建
    NetC1_1
    C1-1
    U1-10
  )

  ```
