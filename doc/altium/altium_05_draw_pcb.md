## 繪製 PCB 流程
- 導入 Schematic 元件到 PCB
  
- 建立規則 + 配置PCB板層數
  - 根據工廠的設計極限建立規則
  - 根據專案的設計建立規則

- 元件布局

- 佈線
  - 建立元件之間的連線 (訊號用)
  - 鋪銅 (電源或接地用)

- PCB 規則檢查

## 環境準備
- File > New > Project

- 建立必要檔案
    - Project 右鍵 > Add New to Project > Schematic Library
    - Project 右鍵 > Add New to Project > PCB Library
    - Project 右鍵 > Add New to Project > Schematic
    - Project 右鍵 > Add New to Project > PCB

- 保存檔案
    - 在專案中， 建立 pcb lib sch 三個目錄
    - 將建立的 Schematic 檔案保存在 sch 目錄中
    - 將建立的 PCB 檔案保存在 pcb 目錄中

- [建立 pcb 元件庫](altium_02_create_pcb_componet.md)

- [建立 sch 元件庫](altium_03_create_sch_componet.md)

- [繪製 Schematic](altium_04_draw_schematic.md)


## 常用快速鍵
- View Configuration，控制顯示的 Layer / 物件，`Ctrl + L`

- 特定網路高亮，例如，將 VCC 的節點高亮，`Ctrl + 需要高亮的接腳`，
  要取消高亮，`Ctrl + 任意空白處`

- 快速重新鋪銅，
  選擇鋪銅 > 滑鼠右鍵 > `Y + A` (Repour All) 或 `Y + R` (Repour Selected)

- 切換直角走線或弧形走線，`Shift + 空白鍵`

- 改變走線的方向，`空白鍵`

- 快速切換 Layer，`Ctrl + Shift + 滾輪`

- 多層 Layer / 單層 Layer 顯示的切換，`Shift + S`
  
## step1，導入 Schematic 元件到 PCB (以 usb2rs485 為例)

  - 注意，導入前，需要確定 *.PcbDoc 的檔案已經建立
  - 點擊 Schematic (*.SchDoc)，並執行 `Design > Update PCB Document` 

    <img src="draw-pcb/add-component-to-pcb-1.png" width=500 height=auto>

  - 點擊 Execute Changes 後，確認所有元件都正常導入，沒有出現錯誤

    <img src="draw-pcb/add-component-to-pcb-2.png" width=1000 height=auto>

  - 導入後，元件會出現在 *.PcbDoc 的檔案中
    
    注意，紫色的外框是 Room，用於批量設計，若不使用可直接刪除

    <img src="draw-pcb/add-component-to-pcb-3.png" width=500 height=auto>

## step2，建立規則 + 配置PCB板層數 (以 usb2rs485 為例)

- 打開 Design Rule
  - 點選 *.PcbDoc
  - 執行 Design > Rules

- [以 常見的工廠製作參數 為例](Design-Guide.md#常見的工廠製作參數)
  
- 規則，調整線距
  - 走線間距 = 0.1mm
  - Design Rules > Electrical > Clearance > Clearance

    <img src="draw-pcb/調整線距.png" width=800 height=auto>
  
- 規則，調整走線寬度
  - 走線寬度 = 0.1mm
  - Design Rules > Routing > Width > Width

    <img src="draw-pcb/調整線寬.png" width=800 height=auto>

- 規則，調整鑽孔內徑/外徑
  - 鑽孔內徑 = 0.2mm
  - 鑽孔外徑 = 0.45mm
  - Design Rules > Routing > Routing Via Style > RoutingVias

    <img src="draw-pcb/調整鑽孔內外徑.png" width=600 height=auto>

- 規則，差分線的布線規則
  - 與最小線寬同
  - Design Rules > Routing > Differential Pairs Routing > DiffPairsRouting

    <img src="draw-pcb/設置差分線布線規則.png" width=800 height=auto>

- 規則，設置鋪銅樣式 (Plane，不切割的一片大銅層)
  - 預設使用散熱的樣式(Relief-Connect)，否則，可直接選擇 Direct Connect
  - Design Rules > Plane > Polygon Connect Style > PolygonConnect

    <img src="draw-pcb/設置鋪銅樣式.png" width=600 height=auto>

- 規則，配置絲印到絲印的距離 (Silk-to-Silk)
  - 預設值為 0.254mm，推薦值為 0.1mm

  <img src="draw-pcb/配置絲印到絲印的距離.png" width=600 height=auto>

- 規則，配置絲印到焊盤的距離 (Silk-to-Solder-Mask)
  - 預設值為 0.254mm，推薦值為 0.1mm 或 0mm

  - 絲印和焊盤的距離過小，會造成絲印和焊盤的重疊，
    
    在實務上，絲印和焊盤的重疊，只會造成部分絲印不完整(部分絲印印在焊盤上)，但並不影響焊盤，因為絲印無法打印在焊盤上，
    因此，可將 Silk-to-Solder-Mask 設置為 0.1mm 或 0mm
  
  <img src="draw-pcb/配置絲印到焊盤的距離.png" width=600 height=auto>

- 規則，最小阻焊層間隙 (焊盤與焊盤的最小間距, Minimum-Solder-Mask-Sliver)
  - 預設值為 0.254mm，推薦值為 0.1mm
  - 一般不使用此規則，設置為 0 對板廠製造不會有影響，可設置為 0mm
  
  <img src="draw-pcb/配置最小阻焊層間隙.png" width=600 height=auto>

- 規則，設置四層板的電源層間距 (本範例未使用)

  <img src="draw-pcb/設置四層板的電源層間距.png" width=600 height=auto>

- 將建立的規則匯出
  
  在規則編輯器中，點擊 Design-Rules 右鍵，選擇 export

  <img src="draw-pcb/export_design_rule.png" width=600 height=auto>

  跳出視窗後，`ctrl+A 選擇要匯出的規則`，按下OK，輸入檔名

- 配置PCB板層數 (雙層板或四層板)

  - 雙層板 (指訊號層只有兩層)
    - Top-Overlay: 頂層絲印層
    - Top-Solder: 頂層阻焊層
      <hr>
    - Top-Layer: 頂層訊號層
    - Dielectric: 介電層(隔絕層)
    - Bottom-Layer: 底層訊號層
      <hr>
    - Bottom-Solder: 底層阻焊層
    - Bottom-Overlay: 底層絲印層

  - 添加 訊號層或鋪銅層

    點選 *.pcbDoc，選擇 Design > Layer-Stack-Manager

    <img src="draw-pcb/add-pcb-layer.png" width=800 height=auto>

## step3，pcb 元件佈局 (以 usb2rs485 為例)

- [布局規則](Design-Guide.md)
  - 規則，芯片電源用的去耦電容，要盡量靠近`芯片電源的接腳`，輸入電源用的去耦電容要靠近`輸入電壓源的管腳`
  - 規則，容值越小的電容，離管腳應該越靠近，容值越大的電容，離管腳的距離可以越遠
  - 規則，`芯片電源用`的去耦電容小，約為 0.1uF，`輸入電源用`的去耦電容大，約為 10uF
  
- 隱藏不需要的 Layer
  - 透過 View Configuration (`Ctrl + L`) 控制隱藏的Layer

  <img src="draw-pcb/hide-layer.png" width=400 height=auto>

- 隱藏不需要的元件名稱
  - 透過 View Configuration (`Ctrl + L`) 控制隱藏的Layer
  - 注意，隱藏後字符有可能因重疊而違反設計規則，布局後需要重新打開隱藏後重新調整位置

  <img src="draw-pcb/hide-component-title.png" width=500 height=auto>
  
- 旋轉 pcb 元件
  
  必須選擇元件後，`按住滑鼠左鍵不放`，`再按下空白鍵`才能進行旋轉

- 布局的原則，
  - 先放置主要芯片
  - 去耦電容
  - 將同功能的元件放在附近，使模擬訊號線和高速訊號線需要盡量的短
  - 注意，pcb 元件的布局是一個動態過程，若佈線不順暢，需要時時修改引腳位置

## step4，pcb 元件佈線 (以 usb2rs485 為例)

- 本範例使用雙層版
  - 頂層 (Top-Layer) 放訊號線和電源，
    - 電源放 Top-Layer 的外圈，且`使用鋪銅作為電源`
    - 鋪銅層不一定要單獨的一層，可以放在訊號層，只要鋪銅的部分不與其他訊號相連接即可

  - 底層 (Bottom-Layer) 做 GND 的鋪銅層，
    在 Top-Layer 的 GND 接腳透過過孔(via) 連接到 Bottom-Layer 的鋪銅層

- 佈線原則
  - step1，佈模擬訊號線，模擬訊號線最容易受干擾，且模擬訊號線需要保證正確性
  - step2，佈高速/高頻訊號線，高速/高頻訊號線容易受到干擾，且容易干擾其他元件
  - step3，佈電源訊號線 或 一般訊號線
  - step4，佈接地線

- 佈線的三種方式
  - 交互式佈線 (Interactive-Routing) : 一次只佈一個訊號線
  - 交互式佈總線 (Interactive-Multi-Routing): 一次佈多個訊號線
  - 交互式佈差分線 (Interactive-Differential-Pair-Routing): 佈差分線用

    <img src="draw-pcb/three-type-routing.png" width=800 height=auto>

- 佈置 usb 差分線
  - 注意，佈置差分線時，需要透過指令，將 usb 定義為差分線，見 [原理圖的繪製](altium_04_draw_schematic.md) 的 step4

  - 選擇 Interactive-Differential-Pair-Routing，

  - 在繪製未完成時，按下 tab鍵 可以調整差分線的參數

    <img src="draw-pcb/set-interaction-differential-pair-routing-property.png" width=300 height=auto>

  - 完成差分線的繪製

    <img src="draw-pcb/complete-differential-pair-routing.png" width=400 height=auto>

- 佈置電源鋪銅
  - 注意，電源走線放 Top-Layer 的外圈，並且`使用鋪銅作為電源的走線`，
    鋪銅會使電源的走線更容易

  - step1，Ctrl + 電源接腳，使所有的電源接腳高亮

  - step2，設置鋪銅
    - 注意，鋪銅時，未設置連接的網路，鋪銅會自動繞開接腳
    - 注意，鋪銅時，若鋪銅需要是連續的，就需要一口描完所有鋪銅的邊

    選擇 `place > polygon pour (多邊形澆灌)`後開始繪製電源的鋪銅層

      - 鋪銅盡量走直角，容易調整位置

      - 可以一次性畫完，也可以將多塊鋪銅利用 combine 組合成一塊
        - 若是一次性畫完，先畫外框，在畫內框
        - 起始點和結束點放在同一點後，按 ESC 結束繪製

          <img src="draw-pcb/polygon-pour-1.png" width=600 height=auto>

      - 不需要畫得很精準，可以事後調整形狀

  - step3，完成初步繪製後，設置鋪銅的 property
    - 設置鋪銅連接的網路
    
    - 選擇 `Pour Over All Same Net Object`，對電源網路的焊盤、導線、鋪銅進行全連接

    - 選擇 `Remove Dead Cooper`，移除未連接的鋪銅

    <img src="draw-pcb/polygon-pour-2.png" width=600 height=auto>

  - 注意，每次對鋪銅進行變更，都需要 Repour All (重新鋪銅) 以修正錯誤，
  
    或在 property 中直接點擊 Repour 命令，
  
    或在選擇鋪銅後，快速鍵 Y -> A

    <img src="draw-pcb/polygon-pour-3.png" width=1000 height=auto>

    
  - 調整鋪銅的形狀
    
    調整方法1，可以使用 `Polygon Actions > combine-seleted-polygons`，將兩塊獨立的鋪銅合併

    <img src="draw-pcb/polygon-pour-4-combine-polygons.png" width=500 height=auto>

    無法產生鋪銅的問題

    > 注意，使用 combine 指令`會產生遮罩用的禁止區(虛線)`，該禁止區會造成無法鋪銅的情況，手動將禁止區刪除即可

    <img src="draw-pcb/polygon-pour-5-forbidden-area.png" width=500 height=auto>

    調整方法2，可以使用 `Polygon Actions > substract-polygons-from-selected` 進行修剪

    > 繪製修剪圖形 > 選擇被修剪的鋪銅 > 右鍵選擇  substract-polygons-from-selected > 選擇修剪圖形 > 滑鼠右鍵執行修剪

    調整方法3，增加頂點

- 佈置高頻的訊號線
  
  有高頻的訊號線，應避免走直角或銳角，參考，[pcb 元件佈線原則](Design-Guide.md#常用設計規則)

  <img src="draw-pcb/connect-high-frequency-signal.png" width=500 height=auto>

- 佈置低頻的訊號線
  
  低頻訊號線可以走折線

  <img src="draw-pcb/connect-low-frequency-signal.png" width=400 height=auto>

- 佈置接地線
  - 注意，在底層(Bottom-Layer)設置 GND 的鋪銅層，
    而Top-Layer 的 GND　訊號，會連接到過孔(Via)後，
    再透過 Via 再連接到 Bottom-Layer 的鋪銅層

    ```mermaid
    flowchart LR
    
    b1["GND\n @ TopView"] --> b2["Via\n@ TopView"]
    b2 --> b3["Via\n@ BottomView"]
    b3 --> b4["GND Plane\n@ BottomView"]
    ```

  - 增加 Via，並設置 Via 連接的網路和相關參數

    <img src="draw-pcb/route-gnd-by-via-1.png" width=800 height=auto>

  - 注意，經過 Via 的走線，會分布在不同 Layer，可以透過` 數字鍵盤的 *` 或 `Ctrl + Shift + 滾輪`，快速切換不同的 Layer

  - 選擇 Interactive-Routing 後，將走線的線境設置為 0.3mm

  - 連接 TopView 的 GND

  - 建立 GND 鋪銅層
    - 切換到 BottomView
    - Place > Polygon Pour
    - 鋪銅過程，參考 [佈置電源鋪銅](altium_05_draw_pcb.md#佈置電源鋪銅)
      
    <img src="draw-pcb/complete-gnd-polygons.png" width=800 height=auto>

## step5，規則檢查
- 選擇規則檢查， `Tools > Design-Rule-Check(DRC)`後，
  點擊左下角的 Run-Design-Rule-Check，執行後會跳出 Summary

  <img src="draw-pcb/design-rule-check-1-summary.png" width=500 height=auto>

- 錯誤，Minimum Solder Mask Sliver (Gap=0.254mm)，阻焊間隙太小 (焊盤間距太小)

- 錯誤，Silk to Silk (Clearance=0.254mm)，絲印與絲印的距離太近
  
  <img src="draw-pcb/drc-1-silk-to-silk-issue.png" width=500 height=auto>
    
- 錯誤，Silk To Solder Mask (Clearance=0.254mm)，絲印到焊盤的距離

- 錯誤，Short-Circuit Constraint Violation，
  > 電源層與接地層短路錯誤

- 錯誤，Net Antennae Violation

- 錯誤，Clearance Constraint Violation

- 以上錯誤，都可以透過 Design-Rule 進行修正，見 [建立規則](#step2建立規則--配置pcb板層數-以-usb2rs485-為例) 一節

## step6，限制 PCB 板的物理大小
- 將板框畫在 Keep-Out Layer
- 切換到 Keep-Out Layer
- 沿著 GND 的鋪銅層畫一層外框，
  
  注意，因為 Keep-Out-Layer 上不能畫任何的東西，
  因此，若出現錯誤，需要對鋪銅層進行 Repour All

- 執行 Design-Rule-Check 確保沒有其他問題

- 選擇 Keep-Out-Layer，按下 Shift + S ，只顯示 Keep-Out-Layer
  
  <img src="draw-pcb/reshape-board-size.png" width=500 height=auto>

- View > 3D Layout Mode 檢視結果

  <img src="draw-pcb/final-pcb-board.png" width=500 height=auto>