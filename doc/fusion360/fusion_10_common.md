
## Ref 
- Usage
  - [Creating an Electronic Component Library in Fusion 360](https://www.youtube.com/watch?v=NITJZfhjppI)
  - [Drawing an Electrical Schematic for a PCB in Fusion 360](https://www.youtube.com/watch?v=flY2tQeuNUk)
  - [Designing a Board Layout for a PCB in Fusion 360](https://www.youtube.com/watch?v=5nLONfdh7vw)

- Simulation
  - [Fusion 360 Schematic SPICE Simulation](https://youtu.be/KXofyaiRbuc?t=402)
  - [Using Fusion 360 to design a simple LED circuit and PCB](https://www.youtube.com/watch?v=Pyjt5I5Y35c)