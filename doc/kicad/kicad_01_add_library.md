
## 修改pcb庫
- step0，開啟 3D View，檢視 pcb 元件的外型

  `View > 3D View` (或 `Alt+3`)

  <img src="common/view-pcb-3dmodel.png" width=500 height=auto>

- step1，複製封裝，並放置封裝原點

  <img src="common/modify-pcb-lib-1.png" width=1000 height=auto>

- step2，雙擊封裝原點，進行封裝屬性的修改

  <img src="common/modify-pcb-lib-2.png" width=700 height=auto>

  也可以直接點擊封裝屬性的按鈕

  <img src="create-pcb-lib/footpring-property-location.png" width=800 height=auto>


## Ref 
- [官方 Library 下載](https://www.kicad.org/libraries/download/) 和 [Repo](https://gitlab.com/kicad/libraries)
  
- [第三方 Library](https://www.kicad.org/libraries/third_party/)

