
## PCB 建庫流程
- 建立元件名
- 放置引腳      (繪製在 頂層， Top-Layer)
- 放置外框      (繪製在 絲印層， Top-Overlay)
- 放置3D外型    (繪製在 機械層， Mechenical-Layer)

## 環境準備
- File > New > Project
  
  自動建立 Schematic (*.kicad_sch) 和 PCB (*.kicad_pcb) 的檔案

- 建立封裝庫 (pcb-library)
  - 新建庫
    > 封裝編輯器 > 檔案 > 新建庫 > 工程 > 輸入封裝庫目錄名 (kicad 會自動添加 .pretty 的後綴)

  - 新增庫 (封裝庫已存在，但未添加到專案中的時候用)
    > 封裝編輯器 > 檔案 > 新增庫

  - 確認或刪除自定義的封裝庫
    > 封裝編輯器 > 偏好設定 > 管理封裝庫 > 工程專用庫

    注意，需要在`管理封裝庫`中添加庫的路徑，才會出現在列表中

## 注意事項
- 在 kicad 中，PCB 編輯器的Y軸，和一般坐標系的方向相反

## 常用快速鍵
- 移動物件，`選擇物件後 > M鍵` (Move)
- 旋轉物件，`選擇物件後 > R鍵` (Rotate)
- 複製物件，`選擇物件後 > D鍵` (Duplicate)
- 變更屬性
  - 方法1，`選擇物件後 > E鍵` (Edit)
  - 方法2，`雙擊物件`
- 測量，`Ctrl + Shift + M` (Measurement)

## PCB 建庫範例 (以 usb2rs485 為例)
- 從 datasheet 檢視元件物理參數
  - 以 [SIT65HVD08](http://www.superesd.com/uploads/20200701/SIT65HVD08-datasheet(EN)_V1.6.pdf) 的 MSOP8 為例

    <img src="create-pcb-lib/SIT65HVD08-msop8.png" width=500 height=auto>

  - B1=5.0    引腳最長距離
  - A2=0.65   引腳中心距離
  - B1=5.0    引腳最長距離
  - A=3       芯片長
  - B=3       芯片寬
  - C1=1.1    芯片高

- step0，進入封裝編輯器

- step1，繪製引腳
  - 1-1，建立新元件
    
    <img src="create-pcb-lib/create-pcb-component.png" width=400 height=auto>

  - 1-2，切換到`頂層銅層(F.Cu)`

  - 1-3，插入 pad

    <img src="create-pcb-lib/add-pad.png" width=500 height=auto>

  - 1-4，變更 pad 的預設樣式

    <img src="create-pcb-lib/modify-pad-property.png" width=700 height=auto>

  - 1-5，變更 Grid 的單位，使物件放在正確的位置
    - B1=5.0    引腳最長距離
    - A2=0.65   引腳中心距離

    <img src="create-pcb-lib/change-grid-style.png" width=500 height=auto>

  - 1-6，測量位置，`Ctrl + Shift + M`

  - 1-7，設置錨點

    <img src="create-pcb-lib/set-anchor.png" width=400 height=auto>

- step2，繪製絲印外框
  - 2-1，切換到`頂層絲印層(F.Silkscreen)`
  - 2-2，放置 > 放置線

- step3，繪製3D外型
  - 3-1，點擊封裝屬性鈕
    
    <img src="create-pcb-lib/footpring-property-location.png" width=800 height=auto>

  - 3-2，3dmodel 庫的預設位置
    
    <img src="create-pcb-lib/find-pcb-component.png" width=500 height=auto>

    或者手動添加 STEP 檔

    <img src="create-pcb-lib/manual-add-3dmodel.png" width=500 height=auto>

  - 3-3，選擇合適的封裝

    <img src="create-pcb-lib/select-3dmodel-from-library.png" width=500 height=auto>

  - 3-4，確保元件的接腳位於焊盤上

    <img src="create-pcb-lib/adjust-pcb-position.png" width=500 height=auto>

## Ref
- [建立封裝元件 @ kicad 中文學院](https://www.youtube.com/watch?v=w5oKyaUiUI4&list=PLOpgBhTFknI66SBHsE2-q6drmVn7KXD5l&index=4)
