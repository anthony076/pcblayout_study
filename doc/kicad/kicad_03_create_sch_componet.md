
## SCH 建庫流程
- 建立 sch 元件
- 放置 sch 元件引腳，並設置引腳屬性
- 繪製 sch 元件的圖形
- 設置 sch 元件屬性

## 環境準備
- File > New > Project
  
  自動建立 Schematic (*.kicad_sch) 和 PCB (*.kicad_pcb) 的檔案

- 建立封裝庫 (pcb-library)
  - 新建庫
    > 封裝編輯器 > 檔案 > 新建庫 > 工程 > 輸入封裝庫目錄名 (kicad 會自動添加 .pretty 的後綴)

  - 新增庫 (封裝庫已存在，但未添加到專案中的時候用)
    > 封裝編輯器 > 檔案 > 新增庫

  - 確認或刪除自定義的封裝庫
    > 封裝編輯器 > 偏好設定 > 管理封裝庫 > 工程專用庫

    注意，需要在`管理封裝庫`中添加庫的路徑，才會出現在列表中

- 建立符號庫 (sch-library)
  - 新建庫
    > 符號編輯器 > 檔案 > 新建庫 > 工程 > 輸入符號庫檔案名 (*.kicad_sym)

  - 新增庫 (符號庫已存在，但未添加到專案中的時候用)
    > 符號編輯器 > 檔案 > 新增庫

  - 確認或刪除自定義的封裝庫已被添加
    > 符號編輯器 > 偏好設定 > 管理封裝庫 > 工程專用庫

    注意，需要在`管理封裝庫`中添加庫的路徑，才會出現在列表中

- [建立 pcb 元件](kicad_02_create_pcb_componet.md)
  
## SCH 建庫範例 (以 usb2rs485 為例)
- step0，進入符號編輯器

- step1，建立新元件

  <img src="create-sch-lib/create-sch-component.png" width=500 height=auto>

  或添加已經存在的 pcb 庫

  <img src="create-sch-lib/import-sch-lib.png" width=500 height=auto>

- step2，放置引腳，並修改引腳屬性

  <img src="create-sch-lib/set-pin-property.png" width=500 height=auto>

- step3，繪製外框

  <font color="red"> 注意，要變更形狀，都必須透過黑色方塊修改 </font>

  選擇形狀 > 點擊滑鼠左鍵1次，指定第1點的位置 > 
  移動滑鼠 > 點擊滑鼠左鍵1次，指定第2點的位置 > 完成繪製

  選擇外框，點擊左鍵兩次，選擇 `填充背景顏色`，讓圖形填充黃色

  <img src="create-sch-lib/fill-shape-with-yellow.png" width=200 height=auto>

- step4，調整引腳名稱的位置，讓引腳名稱出現在方塊內部
  
  <img src="create-sch-lib/modify-pin-name-position.png" width=500 height=auto>

- step5，將對應的 footpring (pcb 元件) 添加到 symbol 中
  
  <img src="create-sch-lib/add-footprint-to-symbol.png" width=500 height=auto>

- step6，添加BOM表相關資訊

  <img src="create-sch-lib/add-BOM-info.png" width=500 height=auto>