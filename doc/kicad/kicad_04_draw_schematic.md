## 繪製 Schematic 流程
- 添加 sch 元件
- 連接 sch 元件
  - 建立 sch元件之間的連線
  - 通過 net-label 連接相同訊號的元件
  - 添加指令標記

- 修改 sch 元件編號
- 檢查 Schematic 

## 環境準備
- File > New > Project
  
  自動建立 Schematic (*.kicad_sch) 和 PCB (*.kicad_pcb) 的檔案

- 建立封裝庫 (pcb-library)
  - 新建庫
    > 封裝編輯器 > 檔案 > 新建庫 > 工程 > 輸入封裝庫目錄名 (kicad 會自動添加 .pretty 的後綴)

  - 新增庫 (封裝庫已存在，但未添加到專案中的時候用)
    > 封裝編輯器 > 檔案 > 新增庫

  - 確認或刪除自定義的封裝庫
    > 封裝編輯器 > 偏好設定 > 管理封裝庫 > 工程專用庫

    注意，需要在`管理封裝庫`中添加庫的路徑，才會出現在列表中

- 建立符號庫 (sch-library)
  - 新建庫
    > 符號編輯器 > 檔案 > 新建庫 > 工程 > 輸入符號庫檔案名 (*.kicad_sym)

  - 新增庫 (符號庫已存在，但未添加到專案中的時候用)
    > 符號編輯器 > 檔案 > 新增庫

  - 確認或刪除自定義的封裝庫已被添加
    > 符號編輯器 > 偏好設定 > 管理封裝庫 > 工程專用庫

    注意，需要在`管理封裝庫`中添加庫的路徑，才會出現在列表中

- [建立 pcb 元件](kicad_02_create_pcb_componet.md)

- [建立 sch 元件](kicad_03_create_sch_componet.md)
  
## 常用快速鍵
- 放置零件，`A鍵`
  
- 進入畫線模式，`W鍵`

## 常用名詞
- 網路標誌 (Net-Label)，
  
  用於宣告將兩個不封閉線段視為同一個訊號，
  兩個使用相同 Net-Label 的訊號，會被視為是`連接在一起`的


## 範例，繪製 Schematic 範例 (以 usb2rs485 為例)

- 修改原理圖大小 + 添加圖紙資訊
  
  <img src="draw-schematic/modify-page-size-and-add-info.png" width=500 height=auto>

- step2，放置零件
  - 方法1，`Place > Add Symbol`
  - 方法2，`A鍵`
  - 方法3，點擊 "Browser-Symbol-Libraries" 按鈕
    
    <img src="draw-schematic/Browser-symbol-librarie-position.png" width=500 height=auto>

- step3，使用常用功能按鈕繪製原理圖
  
  <img src="draw-schematic/common-functions.png" width=200 height=auto>

  注意，不要透過`符號屬性`修改值，應該從 `右鍵 > 編輯 Value 欄位` 修改

  <img src="draw-schematic/dont-modify-value-from-property.png" width=1000 height=auto>

- step4，批量自動添加元件編號 (Annotation Schematic)
  
  `工具 > 批註原理圖`

- step5，驗證 Schematic 是否出現錯誤

  - `檢查 > 電器規則檢查(ERC)`

  - 修正電源引腳的問題，
  
    若出現 `輸入電源引腳不受任何輸出電源引腳的驅動` 的錯誤
    
    - 原因，

      使用電源/接地/輸入電源，需要提供電壓源(例如AC或DC)或電流源的連接，
      否則，ERC 就會拋出電源引腳未驅動的錯誤
    
    - 解法1，若是出現在`電源接腳或接地接腳`，添加 Power_Flag
      
      <img src="draw-schematic/ERC-power-flag.png" width=300 height=auto>
      
      加上 Power_Flag，告訴 ERC 忽略沒有連接到電壓源的錯誤

    - 解法2，若是出現在`芯片電源接腳`，將該引腳的類型，從輸入電源改為無源

- step6，最終結果
  
  <img src="draw-schematic/final-schematic.png" width=800 height=auto>
