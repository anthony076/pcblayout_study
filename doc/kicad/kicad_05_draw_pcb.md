## 繪製 PCB 流程
- 導入 Schematic 元件到 PCB
  
- 建立規則 + 配置PCB板層數
  - 根據工廠的設計極限建立規則
  - 根據專案的設計建立規則

- 元件布局

- 佈線
  - 建立元件之間的連線 (訊號用)
  - 鋪銅 (電源或接地用)

- PCB 規則檢查

## 環境準備
- File > New > Project
  
  自動建立 Schematic (*.kicad_sch) 和 PCB (*.kicad_pcb) 的檔案

- 建立封裝庫 (pcb-library)
  - 新建庫
    > 封裝編輯器 > 檔案 > 新建庫 > 工程 > 輸入封裝庫目錄名 (kicad 會自動添加 .pretty 的後綴)

  - 新增庫 (封裝庫已存在，但未添加到專案中的時候用)
    > 封裝編輯器 > 檔案 > 新增庫

  - 確認或刪除自定義的封裝庫
    > 封裝編輯器 > 偏好設定 > 管理封裝庫 > 工程專用庫

    注意，需要在`管理封裝庫`中添加庫的路徑，才會出現在列表中

- 建立符號庫 (sch-library)
  - 新建庫
    > 符號編輯器 > 檔案 > 新建庫 > 工程 > 輸入符號庫檔案名 (*.kicad_sym)

  - 新增庫 (符號庫已存在，但未添加到專案中的時候用)
    > 符號編輯器 > 檔案 > 新增庫

  - 確認或刪除自定義的封裝庫已被添加
    > 符號編輯器 > 偏好設定 > 管理封裝庫 > 工程專用庫

    注意，需要在`管理封裝庫`中添加庫的路徑，才會出現在列表中

- [建立 pcb 元件](kicad_02_create_pcb_componet.md)

- [建立 sch 元件](kicad_03_create_sch_componet.md)

- [繪製 Schematic](kicad_04_draw_schematic.md)

## 常用快速鍵
- 進入佈線模式，`選擇元件 > X鍵`
- 切換佈線方向，`/鍵`
- 切換佈線模式(斜角或圓角)，`Ctrl + /鍵`
- 放置通孔，`佈線模式 > V鍵`
- 特定訊號高亮，`Ctrl + 接腳`
  
  取消高亮，`Ctrl + 任意空白處`

## 範例，繪製 PCB 範例 (以 usb2rs485 為例)
- 修改原理圖大小 + 添加圖紙資訊
  
  <img src="draw-pcb/modify-page-size-and-add-info.png" width=500 height=auto>

  隱藏圖紙資訊

  <img src="draw-pcb/hide-pcb-draw-frame.png" width=200 height=auto>

- step1，繪製PCB板框的大小
  - 板框的大小，畫在 `Edge.Cuts層`
  - 切換到 `Edge.Cuts層`
  - 使用 繪製線 (Ctrl+Shift+L) 進行繪製，
    
    注意，不要使用佈線進行繪製，佈線是焊盤使用的

    <img src="draw-pcb/draw-board-frame.png" width=500 height=auto>

  - 透過 3D 檢視器 (Alt+3) 檢視結果

- step2，設置 pcblayout 規則和板層
  - 2-1，開啟 pcb板設置
    - 方法1，檔案 > 電路板設定
    - 方法2，點擊 `電路板設定的按鈕`

    <img src="draw-pcb/open-pcbboard-setting.png" width=500 height=auto>

  - 2-2，設置要顯示的層
  
    <img src="draw-pcb/set-show-layer.png" width=300 height=auto>

  - 2-3，設置層數和層厚

    <img src="draw-pcb/set-layer-number-and-thickness.png" width=600 height=auto>

  - 2-4，根據製造商製作能力設置 design-rule-check 的規則

    <img src="draw-pcb/set-manafacture-limitation.png" width=800 height=auto>
    
    也可以自定義每一個網路的規則

    <img src="draw-pcb/set-rule-by-network.png" width=800 height=auto>

  - 2-5，設置阻焊層厚度
    - 阻焊劑位於焊盤的外圍
    - 此設定會覆蓋元件本身的阻焊劑設定，且會套用所有的焊盤

      <img src="draw-pcb/set-mask-thickness.png" width=800 height=auto>

- step3，將 Schematic 的元件導入到 PCB 中
  - 開啟 Schematic
  - 執行 工具 > 從原理圖更新 PCB

- step4，常用功能
  - 使用 mm 單位
  - 顯示飛線
  - 顯示覆銅
  - 關閉輪廓模式

  <img src="draw-pcb/common-funcions-in-pcb-drawing.png" width=800 height=auto>

- step5，隱藏元件編號

  <img src="draw-pcb/hide-component-label.png" width=300 height=auto>

- step6，元件佈局
  - 參考，[design-rules](../design-rules.md) 的 `pcb 元件佈局原則` 一節

  - 選擇元件後，拖曳移動或M鍵移動
  - 選擇元件後，R鍵旋轉
  - 透過 `檢視 > 3D-Viewer` (或 Alt+3) 檢視 3D模型

- step7，元件佈線
  - 參考，[design-rules](../design-rules.md) 的 `pcb 元件佈局原則` 一節

  - 設置走線實際寬度，

    注意，step2 是設置 design-rule-check `判定違規的臨界值`，
    而此步驟是建立`實際佈線`時的走線寬度，理論上應符合 DRC 的限制

    <img src="draw-pcb/define-trace-thickness.png" width=500 height=auto>

  - 設置移除多餘的佈線，同一個接腳繪製新的佈線後，會自動移除舊的

    <img src="draw-pcb/auto-remove-old-trace.png" width=500 height=auto>

- step8，放置 USB 的差分線
  - 注意，要使用差分線前，在 Schematic 的中，`差分線的網路標籤(Net-Label)需要以相同的前綴命名`，例如，USB_P 和 USB_N，否則無法執行差分線佈線的命令

  - `佈線 > 差分線互動佈線 (或按6)`
  
  - 注意，若有可以放置差分線的位置，才會出現差分線，
    盡量走可以產生等長線段的位置，位置偏左或偏右，都有可能無法產生差分線

    <img src="draw-pcb/differential-pair-example.png" width=500 height=auto>

- step9，放置電源覆銅
  - 9-1，新增覆銅

    <img src="draw-pcb/create-copper-plane.png" width=300 height=auto>

  - 9-2，覆銅的設定
    - 指定要連接的訊號
    - 指定覆銅的名稱
    - 只允許直角
    - 選擇覆銅與焊盤連接的樣式
      
      參考，[覆銅與焊盤連接的樣式](../design-rules.md)

    - 刪除孤銅

    <img src="draw-pcb/set-copper-plane.png" width=500 height=auto>
    
  - 9-3，繪製覆銅區，並執行覆銅

    <img src="draw-pcb/execute-copper-filling.png" width=500 height=auto>
    
  - 9-4，合併覆銅

    <img src="draw-pcb/concate-copper-plane.png" width=500 height=auto>

- step10，放置接地用通孔
  - 點選 接腳 > `按X鍵`，進入佈線 
  - 在放置通孔的位置，`按V鍵`放置通孔，並且會自動切換到 Bottom-Layer
  - (選用) 若不需要在 Bottom-Layer 進行繪製，`按ESC鍵` 取消

- step11，放置接地覆銅

- step12，調整元件符號在絲印層的位置

  注意，絲印的位置不要與焊盤重疊
  
  <img src="draw-pcb/highlight-active-layer.png" width=500 height=auto>


- step13，執行 Design-Rules-Check (DRC)
  - 執行 DRC

    <img src="draw-pcb/execute-Design-Rule-Check.png" width=300 height=auto>

  - 錯誤，佈線有未連線端
    
    原因，佈線時，產生多餘的線段，且該線段未建立任何連接
    
    <img src="draw-pcb/DRC-佈線有未連線端.png" width=300 height=auto>
    
    解決方式，刪除該線段即可

  - 錯誤，外框重疊
    
    原因，元件的紫色外框發生重疊
    
    <img src="draw-pcb/DRC-外框重疊.png" width=300 height=auto>
    
    解決方式，重新調整零件位置，紫色外空部分不能重疊

  - 錯誤，過孔外徑錯誤

    原因，元件的過孔外徑，小於約束的最小過孔外徑

    <img src="draw-pcb/DRC-過孔外徑小於約束值.png" width=500 height=auto>

     解決方式，重新設置約束值，或重新調整實際過孔外徑的大小


- 最後結果
  
  <img src="draw-pcb/final-pcb.png" width=500 height=auto>

## 技巧，變寬走線的繪製

  建立預先定義的走線寬度
  
  <img src="draw-pcb/predefined-trace-and-hole.png" width=800 height=auto>

  `按X鍵`，進入佈線模式 > 點選右鍵，`選擇佈線/過孔寬度` > 選擇`預定義的寬度`

  <img src="draw-pcb/variant-width-trace.png" width=500 height=auto>
