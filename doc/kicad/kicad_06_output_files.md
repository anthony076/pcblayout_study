## 輸出相關檔案

- 輸出 Schematic
  
  在Schematic編輯器中，
  
  <img src="output-files/output-schematic.png" width=300 height=auto>

- 輸出 BOM
  
  在Schematic編輯器中，

  <img src="output-files/output-bom.png" width=300 height=auto>

- 輸出 PCB
  
  在PCB編輯器中，

  <img src="output-files/output-pcb.png" width=800 height=auto>

- 輸出 Gerber 和 Drill 檔案
  - 在專案中建立 gerber 的目錄
  - 在PCB編輯器中，`檔案 > 繪製`
  - 輸出 gerber 和 drill 檔案

    <img src="output-files/output-gerber-and-drill.png" width=500 height=auto>

