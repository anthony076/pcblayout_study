## 名詞解釋
- Layer
  - F.cu，頂層鋪銅層或頂層訊號層，F = Front
  - In1.cu，中間鋪銅層或頂層訊號層，In1 = Intermedia-1
  - B.cu，底層鋪銅層或頂層訊號層，B = Bottom
  - F.paste，頂層錫膏層 (焊盤)
  - F.Silkscreen，頂層絲印層
  - F.Mask，頂層阻焊層
  - F.Courtyard，頂層封裝外框
  - F.Fab，頂層封裝裝配
  - User.Drawing，圖形說明
  - User.Comment，註解
  - Edge.Cuts，電路板邊框

## 將 EAGLE 匯入 KiCAD 中

- step1，將 EAGLE 繪製的原理圖(*.sch) 和 PCB(*.brd) 放在同一個目錄中

- step2，以文字檔修改 *.sch
  > 將 NanoV3.3.sch 的 Line-177 改為 <schematic xreflabel="D">

- step3，匯入原理圖
  > 在 原理圖編輯器 中，選擇 檔案 > 匯入 > 非 KiCad 原理圖 > 選擇 *.sch 檔

- step4，選擇空的目錄，用於保存 kicad 專案

- step5，PCB Layer 的映射時，選擇 Auto-Match Layers


## Ref
- [kicad論壇](https://forum.kicad.info/)
  