
## 教學
- [design-guides](doc/design-rules.md) 

- Altium Designer 的使用，以[usb2rs485](usb2rs485) 為例
  - [01. 添加 Library](doc/altium/altium_01_add_library.md)
  
  - [02. 建立 pcb 元件](doc/altium/altium_02_create_pcb_componet.md)
  
  - [03. 建立 sch 元件](doc/altium/altium_03_create_sch_componet.md)

  - [04. 繪製 Schematic](doc/altium/altium_04_draw_schematic.md)

  - [05. 繪製 PCB](doc/altium/altium_05_draw_pcb.md)
 
  - [00. 不分類的教學](doc/altium/altium_00_common.md)

  - [PCB的結構](doc/altium/altium_05_draw_pcb.md) (見 `配置PCB板層數` 一節)

  - [鋪銅範例](doc/altium/altium_05_draw_pcb.md) (見 `佈置電源鋪銅` 一節)
    
  - [電容規格](doc/altium_04_draw_schematic.md) (見 `多層陶瓷電容(MLCC)/貼片電容` 一節)

- KiCAD 的使用
  - 優點: Open-Source + support python + cross-platform
  
  - [01. 添加 Library](doc/kicad/kicad_01_add_library.md)
  
  - [02. 建立 pcb 元件](doc/kicad/kicad_02_create_pcb_componet.md)

  - [03. 建立 sch 元件](doc/kicad/kicad_03_create_sch_componet.md)

  - [04. 繪製 Schematic](doc/kicad/kicad_04_draw_schematic.md)

  - [05. 繪製 PCB](doc/kicad/kicad_05_draw_pcb.md)

  - [06. 輸出相關檔案](doc/kicad/kicad_06_output_files.md)

  - [10. 不分類的教學](doc/kicad/kicad_10_common.md)

- EasyEDA 的使用 (中國產品)
  
## 範例
- usb2rs485-altium
- usb2rs485-kicad
- Arduino-Nano
  
## Ref
- [設計範例 @ oshwlab](https://oshwlab.com/)
- [設計範例 @ 立創](https://item.szlcsc.com/100866.html)